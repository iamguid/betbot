package com.betbot;

import com.betbot.dataCollector.demoFileProcessing.ProcessingManager;
import com.betbot.dataCollector.hltv.GOTVDemoSummary;
import com.betbot.dataCollector.hltv.HltvWebClient;

import org.javatuples.Pair;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;


public class Main {
    public static void main(String[] args) throws InterruptedException, URISyntaxException, IOException, Exception {
        System.out.println("Start hltv demo list parsing...");
        HltvWebClient hltvWebClient = new HltvWebClient();
        List<GOTVDemoSummary> demoSummaries = hltvWebClient.parseGOTVDemosSummaryPage(0);
        System.out.println("Parsing comlete, parsed " + demoSummaries.size() + " demos");

        List<Pair<Integer, String>> downloads = demoSummaries
                .stream()
                .filter(d -> d.hasResult && d.hasStats)
                .map(d -> {
                    Integer hltvId;
                    String URL;

                    try {
                        hltvId = d.hltvDemoId;
                        URL = hltvWebClient.buildDemoDownloadUrl(d.hltvDemoId);
                    } catch (URISyntaxException e) {
                        System.out.print(e.getMessage());
                        return null;
                    }

                    return new Pair<>(hltvId, URL);
                })
                .collect(Collectors.toList());

        List<Pair<Integer, String>> fdownload = new ArrayList<>();
        fdownload.add(downloads.get(0));

        String targetFilesFolder = new File("dems").getAbsolutePath();
        ProcessingManager demoFilesProcessingManager = new ProcessingManager(targetFilesFolder);

        System.out.println("Start hltv demos processing...");
        demoFilesProcessingManager.processDemos(fdownload);

        while (true) {
            System.out.print(buildProgressBar(demoFilesProcessingManager.getProgress()));
            Thread.sleep(1000);
        }
    }

    private static String buildProgressBar(double progress) {
        StringBuilder stringBuilder = new StringBuilder(140);

        stringBuilder.append('\r');
        stringBuilder.append(String.join("", Collections.nCopies(progress == 0 ? 2 : 2 - (int) (Math.log10(progress)), " ")));
        stringBuilder.append(String.format(" %.2f%% [", progress));
        stringBuilder.append(String.join("", Collections.nCopies((int) progress, "=")));
        stringBuilder.append('>');
        stringBuilder.append(String.join("", Collections.nCopies(100 - (int) progress, " ")));
        stringBuilder.append(']');

        return stringBuilder.toString();
    }
}

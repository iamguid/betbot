package com.betbot.dataCollector.hltv;

import org.apache.http.client.utils.URIBuilder;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;


public class HltvWebClient {
    private final static String HLTV_BASE_URL = "http://www.hltv.org/";

    public List<GOTVDemoSummary> parseGOTVDemosSummaryPage(int startPage, int count) throws URISyntaxException, IOException {
        List<GOTVDemoSummary> totalDemosList = new ArrayList<>();

        for (int page = startPage; page < startPage + count; page++) {
            totalDemosList.addAll(parseGOTVDemosSummaryPage(page));
        }

        return totalDemosList;
    }

    public List<GOTVDemoSummary> parseGOTVDemosSummaryPage(int pageNumber) throws URISyntaxException, IOException {
        Map<String, String> props = new HashMap<>();
        props.put("pageNumber", Integer.toString(pageNumber));
        String url = buildURL(Page.GOTV_DEMOS_SUMMARY_LIST, props);

        Document doc = configureJsoupConnect(url).get();
        Element contentRoot = doc.getElementsByClass("covResultBoxContent").get(1);
        Elements dataNodes = contentRoot.child(1).select("div[style=padding-top:8px;]");

        List<GOTVDemoSummary> demosList = new ArrayList<>();
        for (Element dataNode : dataNodes) {

            GOTVDemoSummary demoSummary = new GOTVDemoSummary();
            demoSummary.hltvDemoId = Integer.parseInt(dataNode.child(0).select("a").get(0).attr("href").split("=")[3]);
            demoSummary.hasResult = dataNode.child(1).select("img").attr("alt").equals("check");
            demoSummary.hasStats = dataNode.child(4).select("img").attr("alt").equals("check");

            demosList.add(demoSummary);
        }

        return demosList;
    }

    public String buildDemoDownloadUrl(int hltvDemoId) throws URISyntaxException {
        Map<String, String> props = new HashMap<>();
        props.put("demoId", Integer.toString(hltvDemoId));
        return buildURL(Page.DOWNLOAD_FILE_PAGE, props);
    }

    private static String buildURL(Page page, Map<String, String> props) throws URISyntaxException {
        URIBuilder builder = new URIBuilder(HLTV_BASE_URL);

        switch (page) {
            case GOTV_DEMOS_SUMMARY_LIST:
                int pageNumber = Integer.parseInt(props.get("pageNumber"));
                int pageOffset = pageNumber * 25;

                builder
                        .addParameter("pageid", "28")
                        .addParameter("offset", Integer.toString(pageOffset))
                        .build();

                break;

            case GOTV_DEMO_INFO:
                builder
                        .addParameter("pageid", "28")
                        .addParameter("demoid", props.get("demoId"))
                        .build();
                break;

            case DOWNLOAD_FILE_PAGE:
                builder
                        .setPath("/interfaces/download.php")
                        .addParameter("demoid", props.get("demoId"));
        }

        return builder.toString();
    }

    private static Connection configureJsoupConnect(String url) {
        return Jsoup.connect(url)
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                .header("Accept-Encoding", "gzip, deflate, sdch")
                .header("Accept-Language", "en-US,en;q=0.8,ru;q=0.6")
                .header("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
    }

    private enum Page {
        GOTV_DEMOS_SUMMARY_LIST, GOTV_DEMO_INFO, DOWNLOAD_FILE_PAGE;
    }
}

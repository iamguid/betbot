package com.betbot.dataCollector.demoFileProcessing;

import org.javatuples.Pair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProcessingManager {
    private final static int MAX_POOL_SIZE = 10;
    private final static int KEEP_ALIVE_TIME = 1000;

    private String demoFilesFolder;
    private List<ProcessUnit> processUnits;
    private TaskExecutionManager downloadFileTaskManager;
    private TaskExecutionManager extractFilesTaskManager;
    private Watcher watcher;

    public ProcessingManager(String demoFilesFolder) throws Exception {
        this.demoFilesFolder = demoFilesFolder;
        this.processUnits = new ArrayList<>();
        this.downloadFileTaskManager = new TaskExecutionManager(MAX_POOL_SIZE, KEEP_ALIVE_TIME);
        this.extractFilesTaskManager = new TaskExecutionManager(MAX_POOL_SIZE, KEEP_ALIVE_TIME);
        this.watcher = new Watcher(this);
        this.watcher.start();
    }

    public void processDemos(List<Pair<Integer, String>> demos) throws Exception {
        for (Pair<Integer, String> d : demos) {
            int id = d.getValue0();
            String sourceURL = d.getValue1();

            ProcessUnit processUnit = new ProcessUnit(id, sourceURL);
            processUnits.add(processUnit);
        }
    }

    public double getProgress() {
        double totalProgress = processUnits.size() * ProcessUnit.ProcessUnitStage.values().length * 100;
        double currentProgress = 0;

        for (ProcessUnit unit : processUnits) {
            if (unit.currentStage == ProcessUnit.ProcessUnitStage.DOWNLOADING) {
                currentProgress += unit.currentTask.getProgress();
            } else if (unit.currentStage == ProcessUnit.ProcessUnitStage.UNPACKING) {
                currentProgress += 100 + unit.currentTask.getProgress();
            }
        }

        return currentProgress / totalProgress * 100;
    }

    private class Watcher extends Thread {
        private ProcessingManager processingManager;

        public Watcher(ProcessingManager processingManager) {
            this.processingManager = processingManager;
        }

        public void run() {
            while (true) {
                this.processingManager.processUnits.forEach(u -> {
                    if (u.currentStage == null || (u.currentTask.isCompleted() && u.currentStage != ProcessUnit.ProcessUnitStage.UNPACKING)) {
                        try {
                            this.nextStage(u);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        private void nextStage(ProcessUnit processUnit) {
            try {
                if (processUnit.currentStage == null) {
                    changeStage(processUnit, ProcessUnit.ProcessUnitStage.DOWNLOADING);
                } else if (processUnit.currentStage == ProcessUnit.ProcessUnitStage.DOWNLOADING) {
                    changeStage(processUnit, ProcessUnit.ProcessUnitStage.UNPACKING);
                } else if (processUnit.currentStage == ProcessUnit.ProcessUnitStage.UNPACKING) {
                    throw new Exception(String.format("Task dose not have next stage"));
                }

                if (processUnit.currentTask != null && processUnit.currentTask.isCancelled()) {
                    throw new Exception(String.format("Task %1$s with id %2$s has cancelled",
                            processUnit.currentStage.toString(),
                            processUnit.id));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void changeStage(ProcessUnit processUnit, ProcessUnit.ProcessUnitStage stage) throws IOException, InterruptedException {
            String sourceURL = processUnit.sourceURL;
            Integer processUnitId = processUnit.id;
            File targetFile = new File(this.processingManager.demoFilesFolder, processUnitId.toString() + ".rar");

            switch (stage) {
                case DOWNLOADING:
                    DownloadFileTask downloadFileTask = new DownloadFileTask(sourceURL, targetFile);
                    processUnit.currentStage = ProcessUnit.ProcessUnitStage.DOWNLOADING;
                    processUnit.currentTask = downloadFileTask;
                    this.processingManager.downloadFileTaskManager.executeTask(downloadFileTask);
                    break;

                case UNPACKING:
                    File targetFilesDir = new File(this.processingManager.demoFilesFolder, processUnitId.toString());
                    ExtractFilesTask extractFilesTask = new ExtractFilesTask(targetFile, targetFilesDir);
                    processUnit.currentStage = ProcessUnit.ProcessUnitStage.UNPACKING;
                    processUnit.currentTask = extractFilesTask;
                    this.processingManager.extractFilesTaskManager.executeTask(extractFilesTask);
                    break;
            }
        }
    }
}

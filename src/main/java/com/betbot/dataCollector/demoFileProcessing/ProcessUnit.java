package com.betbot.dataCollector.demoFileProcessing;

public class ProcessUnit {
    public int id;
    public String sourceURL;
    public ProcessUnitStage currentStage;
    public Task currentTask;

    public ProcessUnit(int id, String sourceURL) {
        this.id = id;
        this.sourceURL = sourceURL;

        this.currentStage = null;
        this.currentTask = null;
    }

    public enum ProcessUnitStage  {
        DOWNLOADING, UNPACKING;
    }
}

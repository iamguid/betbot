package com.betbot.dataCollector.demoFileProcessing;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.*;

public class DownloadFileTask extends Task {
    private static final int STREAM_BUFFER_SIZE = 1024;

    private final String sourceURL;
    private final File targetFile;
    private boolean resuming;
    private long totalBytes;
    private long downloadedBytes;

    public DownloadFileTask(String sourceURL, File targetFile) throws IOException, InterruptedException {
        this.sourceURL = sourceURL;
        this.targetFile = this.prepareFile(targetFile);

        this.resuming = this.targetFile.length() > 0;
        this.downloadedBytes = this.resuming ? this.targetFile.length() : 0;
        this.totalBytes = 0;

        setState(TaskState.PENDING);
    }

    public void run() {
        setState(TaskState.IN_PROGRESS);

        BufferedInputStream inputStream = null;
        BufferedOutputStream outputStream = null;
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;

        try {
            httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(sourceURL);
            response = httpClient.execute(httpGet);

            // Make sure response code is in the 200 range.
            int resStatusCode = response.getStatusLine().getStatusCode();
            if (resStatusCode / 100 != 2) {
                throw new HttpResponseException(resStatusCode, "Bad status code");
            }

            // Get hold of the response entity
            HttpEntity entity = response.getEntity();

            // Check for valid content length.
            totalBytes = entity.getContentLength();
            if (totalBytes < 1) {
                throw new HttpException("Response content length is empty");
            }

            // Check file already downloaded
            if (downloadedBytes == totalBytes) {
                setState(TaskState.COMPLETED);

                // TODO: Can I do that?
                return;
            }

            inputStream = new BufferedInputStream(entity.getContent(), STREAM_BUFFER_SIZE);
            outputStream = new BufferedOutputStream(new FileOutputStream(targetFile), STREAM_BUFFER_SIZE);

            int inByte = 0;
            while((inByte = inputStream.read()) != -1) {
                outputStream.write(inByte);
                downloadedBytes++;
            }

            if (downloadedBytes < totalBytes) {
                throw new HttpException("File download failed");
            }

            setState(TaskState.COMPLETED);
        } catch (Exception e) {
            setState(TaskState.CANCELLED);
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public float getProgress() {
        if (downloadedBytes < 0 || totalBytes < 1) {
            return 0;
        }

        return ((float) downloadedBytes / totalBytes) * 100;
    }

    private static File prepareFile(File file) throws IOException, InterruptedException {
        File dirPath = new File(file.getParent());
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }

        if (!file.exists()) {
            file.createNewFile();
        }

        return file;
    }
}

package com.betbot.dataCollector.demoFileProcessing;

import java.util.concurrent.*;

public class TaskExecutionManager{
    private LinkedBlockingQueue<Runnable> workQueue;

    private ExecutorService tasksThreadPool;

    public TaskExecutionManager(int maxPoolSize, int keepAliveTime) {
        workQueue = new LinkedBlockingQueue<>();

        tasksThreadPool = new ThreadPoolExecutor(
                maxPoolSize, maxPoolSize,
                keepAliveTime, TimeUnit.MILLISECONDS,
                workQueue
        );
    }

    public void executeTask(Task task) throws InterruptedException {
        tasksThreadPool.execute(task);
    }

    public LinkedBlockingQueue<Runnable> getTasksQueue() {
//        // TODO: What this shit
//        DownloadFileTask[] castedTasks = workQueue.toArray(new DownloadFileTask[0]);
//        return Arrays.asList(castedTasks);

        return workQueue;
    }
}

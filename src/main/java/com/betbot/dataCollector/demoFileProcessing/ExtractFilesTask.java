package com.betbot.dataCollector.demoFileProcessing;

import com.github.junrar.Archive;
import com.github.junrar.impl.FileVolumeManager;
import com.github.junrar.rarfile.FileHeader;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ExtractFilesTask extends Task {
    private final File sourceFile;
    private final File targetFilesPath;
    private List<File> unpackedFiles;
    private int totalFilesCount;

    public ExtractFilesTask(File sourceFile, File targetFilesPath) {
        this.sourceFile = sourceFile;
        this.targetFilesPath = targetFilesPath;
        this.totalFilesCount = 0;
        this.unpackedFiles = new ArrayList<>();
        setState(TaskState.PENDING);
    }

    public void run() {
        setState(TaskState.IN_PROGRESS);

        try {
            Archive archive = new Archive(new FileVolumeManager(this.sourceFile));
            totalFilesCount = archive.getFileHeaders().size();

            FileHeader fileHeader = archive.nextFileHeader();
            while (fileHeader != null) {
                File outFile = new File(this.targetFilesPath, fileHeader.getFileNameString().trim());
                prepareTargetFileDir(outFile);

                FileOutputStream fileOutputStream = new FileOutputStream(outFile);
                archive.extractFile(fileHeader, fileOutputStream);
                fileOutputStream.close();
                fileHeader = archive.nextFileHeader();

                unpackedFiles.add(outFile);
            }

            setState(TaskState.COMPLETED);
        } catch (Exception e) {
            setState(TaskState.CANCELLED);
            e.printStackTrace();
        }
    }

    public List<File> getUnpackedFiles() {
        return unpackedFiles;
    }

    public float getProgress() {
        int unpackedFilesCount = unpackedFiles.size();

        if (unpackedFilesCount <= 0 || totalFilesCount < 1) {
            return 0;
        }

        return ((float) unpackedFilesCount / totalFilesCount) * 100;
    }

    private static File prepareTargetFileDir(File targetFile) throws IOException {
        File dirPath = new File(targetFile.getParent());
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }

        if (targetFile.exists()) {
            targetFile.delete();
        }

        targetFile.createNewFile();

        return targetFile;
    }
}

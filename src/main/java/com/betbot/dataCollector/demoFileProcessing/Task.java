package com.betbot.dataCollector.demoFileProcessing;

public abstract class Task implements Runnable {
    private TaskState state;

    public boolean isCompleted() {
        return state == TaskState.COMPLETED;
    }

    public boolean isPending() {
        return state == TaskState.PENDING;
    }

    public boolean isCancelled() {
        return state == TaskState.CANCELLED;
    }

    public boolean isInProgress() {
        return state == TaskState.IN_PROGRESS;
    }

    public abstract float getProgress();

    protected void setState(TaskState state) {
        this.state = state;
    }

    enum TaskState {
        IN_PROGRESS, PENDING, COMPLETED, CANCELLED;
    }
}
